import React from 'react';
import { BrowserRouter, Switch, Route } from 'react-router-dom';

import Main from './pages/Main';
import Auth from './pages/Auth';

function App() {
  return (
    <div className="App">
      <BrowserRouter>
        <Switch>
          <Route path="/" exact component={Main}/>
          <Route path="/auth" component={Auth} />
        </Switch>
      </BrowserRouter>
    </div>
  );
}

export default App;
