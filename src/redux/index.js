import { createStore } from 'redux'

import authReducer from './Authentication/reducer';

const store = createStore(authReducer);

export default store;