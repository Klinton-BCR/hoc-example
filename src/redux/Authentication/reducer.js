const initialState = {
  profile: {
    name: 'User',
    role: 'admin'
  }
}

export default function (state = initialState, action) {
  switch (action.type) {
    default:
      return state
  }
}