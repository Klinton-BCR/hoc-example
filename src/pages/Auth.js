import React from 'react';
import withPermission from '../utils/withPermission';

const Auth = () => {
  return <h1>Auth</h1>
}

export default withPermission(['admin', 'customer'], '/')(Auth);