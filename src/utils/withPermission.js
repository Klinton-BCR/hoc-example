import React from 'react'
import { useSelector } from 'react-redux'
import { Redirect } from 'react-router-dom'

const withPermission = (roles, redirect = '/') =>
  (Component) => (props) => {
    const role = useSelector(state => state.profile.role)

    return roles.includes(role)
      ? <Component {...props} />
      : <Redirect to={redirect} />
  }

export default withPermission